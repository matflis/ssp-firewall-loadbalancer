package pl.ssp.db;

import org.projectfloodlight.openflow.types.IPv4Address;
import org.projectfloodlight.openflow.types.TransportPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public final class DbAccessService {

    private final static Logger logger = LoggerFactory.getLogger(DbAccessService.class);

    private final static String CONNECTION_STRING = "jdbc:sqlite:test.db";
    private final static String QUERY = "SELECT COUNT(*) FROM BLOCKED_ADDRESS WHERE SOURCE_ADDRESS = ? AND " +
            "DESTINATION_ADDRESS = ? AND PORT = ? OR SOURCE_ADDRESS = '*' AND DESTINATION_ADDRESS = ? AND PORT = ?";

    private final static DbAccessService INSTANCE = new DbAccessService();

    private DbAccessService() {}

    public static DbAccessService getInstance() { return INSTANCE; }

    public Long getMatchesCount(final IPv4Address sourceAddress, final IPv4Address destinationAddress, final TransportPort port) {
        try(Connection connection = DriverManager.getConnection(CONNECTION_STRING);
            PreparedStatement statement = connection.prepareStatement(QUERY)) {
            statement.setString(1, sourceAddress.toString());
            statement.setString(2, destinationAddress.toString());
            statement.setInt(3, port.getPort());
            statement.setString(4, destinationAddress.toString());
            statement.setInt(5, port.getPort());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getLong(1);
        } catch (SQLException e) {
            logger.error("An error occurred while checking if flow is valid. {}", e.getMessage());
        }
        // This number is returned as indication that exception occurred
        return 1L;
    }
}