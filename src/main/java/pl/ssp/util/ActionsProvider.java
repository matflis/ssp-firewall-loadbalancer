package pl.ssp.util;

import org.projectfloodlight.openflow.protocol.OFFactory;
import org.projectfloodlight.openflow.protocol.action.OFAction;
import org.projectfloodlight.openflow.protocol.action.OFActionOutput;
import org.projectfloodlight.openflow.protocol.action.OFActionSetField;
import org.projectfloodlight.openflow.protocol.action.OFActions;
import org.projectfloodlight.openflow.protocol.oxm.OFOxms;
import org.projectfloodlight.openflow.types.IPv4Address;
import org.projectfloodlight.openflow.types.MacAddress;
import org.projectfloodlight.openflow.types.OFPort;

import java.util.Arrays;
import java.util.List;

public final class ActionsProvider {

    private ActionsProvider() {}

    // Taken from examples
    // https://floodlight.atlassian.net/wiki/display/floodlightcontroller/How+to+use+OpenFlowJ-Loxigen#HowtouseOpenFlowJ-Loxigen-OpenFlow1.0

    public static List<OFAction> getLoadBalancingActions(String lowestLoadedServerIp, DeviceInfo bestDeviceInfo, OFFactory ofFactory) {
        final OFActions actions = ofFactory.actions();
        final OFOxms oxms = ofFactory.oxms();
        final OFActionSetField changeMacAction = actions.buildSetField()
                .setField(
                        oxms.buildEthDst()
                                .setValue(MacAddress.of(bestDeviceInfo.getMac()))
                                .build()
                ).build();

        final OFActionSetField changeIPDstAction = actions.buildSetField()
                .setField(
                        oxms.buildIpv4Dst()
                                .setValue(IPv4Address.of(lowestLoadedServerIp))
                                .build()
                ).build();

        return Arrays.asList(changeMacAction, changeIPDstAction, getOutPortAction(bestDeviceInfo.getPort(), actions));
    }

    public static List<OFAction> getSourceChangingActions(String ipAddress, String mac, int port, OFFactory ofFactory) {
        final OFActions actions = ofFactory.actions();
        final OFOxms oxms = ofFactory.oxms();
        final OFActionSetField changeMacAction = actions.buildSetField()
                .setField(
                        oxms.buildEthSrc()
                                .setValue(MacAddress.of(mac))
                                .build()
                ).build();

        final OFActionSetField changeIPDstAction = actions.buildSetField()
                .setField(
                        oxms.buildIpv4Src()
                                .setValue(IPv4Address.of(ipAddress))
                                .build()
                ).build();

        return Arrays.asList(changeMacAction, changeIPDstAction, getOutPortAction(port, actions));
    }

    private static OFActionOutput getOutPortAction(int port, OFActions actions) {
        return actions.buildOutput()
                    .setMaxLen(0xff_ff_ff_ff)
                    .setPort(OFPort.of(port))
                    .build();
    }
}