package pl.ssp.util;

import net.floodlightcontroller.core.IOFSwitch;
import org.projectfloodlight.openflow.protocol.OFFlowAdd;
import org.projectfloodlight.openflow.protocol.action.OFAction;
import org.projectfloodlight.openflow.protocol.match.Match;
import org.projectfloodlight.openflow.types.OFBufferId;
import org.projectfloodlight.openflow.types.OFPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public final class FlowInjectorService {

    private final static Logger logger = LoggerFactory.getLogger(FlowInjectorService.class);

    private final static int FLOWMOD_DEFAULT_IDLE_TIMEOUT = 2;
    private final static int FLOWMOD_DEFAULT_HARD_TIMEOUT = 5;

    private final static FlowInjectorService INSTANCE = new FlowInjectorService();

    private FlowInjectorService() {}

    public static FlowInjectorService getInstance() { return INSTANCE; }

    public void injectFlow(IOFSwitch sw, Match filteringMatch, OFBufferId bufferId, int port, List<OFAction> actionList) {
        final OFFlowAdd flowAddAction = sw.getOFFactory().buildFlowAdd()
                .setMatch(filteringMatch)
                .setIdleTimeout(FLOWMOD_DEFAULT_IDLE_TIMEOUT)
                .setHardTimeout(FLOWMOD_DEFAULT_HARD_TIMEOUT)
                .setBufferId(bufferId)
                .setOutPort(OFPort.of(port))
                .setPriority(ThreadLocalRandom.current().nextInt(1000, 100000 + 1))
                .setActions(actionList)
                .build();
        try {
            logger.info("Trying to write new FlowAdd message: {}", flowAddAction);
            sw.write(flowAddAction);
            logger.info("Flow written successfully");
        } catch (Exception e) {
            logger.error("Writing new flow to the switch failed {}", e.getMessage(), e);
        }
    }
}
