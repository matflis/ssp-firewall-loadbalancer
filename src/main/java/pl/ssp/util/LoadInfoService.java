package pl.ssp.util;

import com.google.common.collect.ImmutableMap;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.net.URL;
import java.util.Comparator;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public final class LoadInfoService {

    private final static Logger logger = LoggerFactory.getLogger(LoadInfoService.class);

    private final static String LOAD_SERVICE_URL = "http://localhost:3000";
    private static final int INTERVAL = 10_000;

    private final static LoadInfoService INSTANCE = new LoadInfoService();

    private final Map<String, Integer> ipToLoad = new ConcurrentHashMap<>(ImmutableMap.of(
            "10.0.0.11", 0, "10.0.0.12", 0, "10.0.0.13", 0
    ));

    public static LoadInfoService getInstance() { return INSTANCE; }

    private LoadInfoService() {
        final Timer t = new Timer(true);
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                ipToLoad.replaceAll((k, v) -> {
                    try(final InputStream input = new URL(LOAD_SERVICE_URL).openStream()) {
                        final Map<String, String> map = new Gson().fromJson(IOUtils.toString(input, "UTF-8"),
                                new TypeToken<Map<String, String>>() {}.getType());
                        logger.debug("New value for server {} : {}", k, map.toString());
                        return Integer.valueOf(map.getOrDefault("value", "0"));
                    }
                    catch (Exception e) {
                        logger.error("An error occurred while trying to get new server load {}", e.getMessage());
                    }
                    // Default value when exception occurs
                    return 0;
                });
            }
        }, 1000, INTERVAL);
    }

    public String getLowestLoadedServer() {
        logger.info("Current server load values: {}", ipToLoad.entrySet().stream()
                .map((e -> String.format("'%s': %s %%", e.getKey(), e.getValue())))
                .collect(Collectors.joining(" | ")));
        return ipToLoad.entrySet().stream() //
                .min(Comparator.comparingInt(Map.Entry::getValue)) //
                .map(Map.Entry::getKey) //
                .orElseThrow(() -> new IllegalStateException("Cannot find lowest value"));
    }
}