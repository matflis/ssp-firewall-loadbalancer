package pl.ssp.util;

public final class DeviceInfo {
    private final String mac;
    private final int port;

    public DeviceInfo(String mac, int port) {
        this.mac = mac;
        this.port = port;
    }

    public String getMac() {
        return mac;
    }

    public int getPort() {
        return port;
    }
}
