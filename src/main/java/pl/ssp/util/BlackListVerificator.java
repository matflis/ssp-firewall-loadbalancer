package pl.ssp.util;

import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.packet.IPv4;
import net.floodlightcontroller.packet.TCP;
import net.floodlightcontroller.packet.UDP;
import org.projectfloodlight.openflow.types.EthType;
import org.projectfloodlight.openflow.types.IPv4Address;
import org.projectfloodlight.openflow.types.IpProtocol;
import org.projectfloodlight.openflow.types.TransportPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.ssp.db.DbAccessService;

import java.util.Optional;
import java.util.function.Function;

public final class BlackListVerificator {

    private final static Logger logger = LoggerFactory.getLogger(BlackListVerificator.class);

    private static final BlackListVerificator INSTANCE = new BlackListVerificator();
    private final DbAccessService dbAccessService = DbAccessService.getInstance();

    private BlackListVerificator() {}

    public static BlackListVerificator getInstance() {
        return INSTANCE;
    }

    public boolean verifyFlow(FloodlightContext context) {
        final Ethernet ethData = IFloodlightProviderService.bcStore.get(context, IFloodlightProviderService.CONTEXT_PI_PAYLOAD);

        if(!isFlowAllowed(ethData)) {
            logger.info("A restricted flow has entered the controller. It will be dropped. {}", ethData.toString());
            return false;
        }
        logger.info("Flow is not restricted. It will be forwarded");
        return true;
    }

    private boolean isFlowAllowed(Ethernet ethData) {
        final Optional<TransportPort> destinationPortOptional = getDestinationPort(ethData);
        if(destinationPortOptional.isPresent()) {
            final IPv4Address sourceAddress = getSourceAddress(ethData);
            final IPv4Address destinationAddress = getDestinationAddress(ethData);
            return dbAccessService.getMatchesCount(sourceAddress, destinationAddress, destinationPortOptional.get()) == 0;
        }
        return true;
    }

    private Optional<TransportPort> getDestinationPort(Ethernet ethData) {
        if(ethData.getEtherType() == EthType.IPv4) {
            final IPv4 iPv4Payload = (IPv4) ethData.getPayload();
            final IpProtocol ipProtocol = iPv4Payload.getProtocol();

            if(ipProtocol.equals(IpProtocol.TCP)) {
                final TCP tcpPayload = (TCP) iPv4Payload.getPayload();
                return Optional.of(tcpPayload.getDestinationPort());
            }
            else if(ipProtocol.equals(IpProtocol.UDP)) {
                final UDP udpPayload = (UDP) iPv4Payload.getPayload();
                return Optional.of(udpPayload.getDestinationPort());
            }
        }
        return Optional.empty();
    }

    private IPv4Address getSourceAddress(Ethernet eth) {
        return getAddress(eth, IPv4::getSourceAddress, true);
    }

    private IPv4Address getDestinationAddress(Ethernet eth) {
        return getAddress(eth, IPv4::getDestinationAddress, false);
    }

    private IPv4Address getAddress(Ethernet eth, Function<IPv4, IPv4Address> ipAddressSupplier, boolean isSource) {
        if(eth.getEtherType() == EthType.IPv4) {
            final IPv4 iPv4 = (IPv4) eth.getPayload();
            final IPv4Address ipv4Address = ipAddressSupplier.apply(iPv4);
            logger.debug("Obtained {} address IP from Ipv4 EthType is: {}", isSource ? "source" : "destination", ipv4Address);
            return ipv4Address;
        }
        throw new IllegalStateException("Cannot extract destination address ip from" + eth.getEtherType() + " ether type.");
    }
}