package pl.ssp.module;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.core.IOFMessageListener;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.core.module.FloodlightModuleException;
import net.floodlightcontroller.core.module.IFloodlightModule;
import net.floodlightcontroller.core.module.IFloodlightService;
import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.packet.IPv4;
import net.floodlightcontroller.util.OFMessageUtils;
import org.projectfloodlight.openflow.protocol.OFMessage;
import org.projectfloodlight.openflow.protocol.OFPacketIn;
import org.projectfloodlight.openflow.protocol.OFType;
import org.projectfloodlight.openflow.protocol.action.OFAction;
import org.projectfloodlight.openflow.protocol.match.Match;
import org.projectfloodlight.openflow.protocol.match.MatchField;
import org.projectfloodlight.openflow.types.EthType;
import org.projectfloodlight.openflow.types.IPv4Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.ssp.util.ActionsProvider;
import pl.ssp.util.DeviceInfo;
import pl.ssp.util.FlowInjectorService;
import pl.ssp.util.LoadInfoService;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class LoadBalancerModule implements IOFMessageListener, IFloodlightModule {

    private final static Logger logger = LoggerFactory.getLogger(LoadBalancerModule.class);
    private final static String EXTERNAL_ADDRESS = "10.0.0.11";

    private IFloodlightProviderService floodlightProviderService;
    private final LoadInfoService loadInfoService = LoadInfoService.getInstance();
    private final FlowInjectorService flowInjectorService = FlowInjectorService.getInstance();

    private final Map<String, DeviceInfo> hostNodesToDeviceInfoMap = ImmutableMap.of(
            "10.0.0.1", new DeviceInfo("00:00:00:00:00:01", 1),
            "10.0.0.2", new DeviceInfo("00:00:00:00:00:02", 2)
    );

    private final Map<String, DeviceInfo> cdNodesToDeviceInfoMap = ImmutableMap.of(
            EXTERNAL_ADDRESS, new DeviceInfo("00:00:00:00:00:11", 3),
            "10.0.0.12", new DeviceInfo("00:00:00:00:00:12", 4),
            "10.0.0.13", new DeviceInfo("00:00:00:00:00:13", 5)
    );

    private static final List<String> internalAddresses = ImmutableList.of("10.0.0.12", "10.0.0.13");

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }

    @Override
    public boolean isCallbackOrderingPrereq(OFType type, String name) {
        return false;
    }

    @Override
    public boolean isCallbackOrderingPostreq(OFType type, String name) {
        return false;
    }

    @Override
    public Command receive(IOFSwitch sw, OFMessage msg, FloodlightContext cntx) {
        logger.info("Incoming packet on LoadBalancerModule");
        final Ethernet ethDataIn = IFloodlightProviderService.bcStore.get(cntx, IFloodlightProviderService.CONTEXT_PI_PAYLOAD);
        if(shouldBeLoadBalanced(ethDataIn)) {
            final IPv4 iPv4Packet = (IPv4) ethDataIn.getPayload();
            final IPv4Address iPv4DestinationAddress = iPv4Packet.getDestinationAddress();
            if(isWithinInternal(iPv4DestinationAddress.toString())) {
                logger.info("Stopping flow. Restricted IP used only for load-balancing has been accessed: {}",
                        iPv4DestinationAddress.toString());
                return Command.STOP;
            }
            final String lowestLoadedServerIp = loadInfoService.getLowestLoadedServer();
            logger.info("Flow will be load balanced. It will be forwarded to the IP: {} ", lowestLoadedServerIp);

            // Inject load balancing flow
            injectLoadBalancingFlow(sw, (OFPacketIn) msg, ethDataIn, lowestLoadedServerIp);

            return Command.STOP;
        } else if(fromInternalAddress(ethDataIn)) {
            final IPv4Address iPv4DestinationAddress = ((IPv4) ethDataIn.getPayload()).getSourceAddress();
            logger.info("Src address '{}' from internal server will be changed.", iPv4DestinationAddress.toString());
            // Inject source-changing flow
            injectSourceChangingFlow(sw, (OFPacketIn) msg, ethDataIn);

            return Command.STOP;
        }
        return Command.CONTINUE;
    }

    private boolean shouldBeLoadBalanced(Ethernet ethData) {
        if(ethData.getEtherType() == EthType.IPv4) {
            final IPv4 iPv4 = (IPv4) ethData.getPayload();
            return cdNodesToDeviceInfoMap.containsKey(iPv4.getDestinationAddress().toString());
        }
        return false;
    }

    private boolean isWithinInternal(String ipvAddress) {
        return internalAddresses.contains(ipvAddress);
    }

    private void injectLoadBalancingFlow(IOFSwitch sw, OFPacketIn msg, Ethernet ethDataIn, String lowestLoadedServerIp) {
        final DeviceInfo bestDeviceInfo = cdNodesToDeviceInfoMap.get(lowestLoadedServerIp);
        final IPv4 iPv4Packet = (IPv4) ethDataIn.getPayload();

        final Match filteringMatch = filteringMatch(sw, msg, ethDataIn, iPv4Packet);
        final List<OFAction> actionList = ActionsProvider.getLoadBalancingActions(lowestLoadedServerIp, bestDeviceInfo, sw.getOFFactory());

        flowInjectorService.injectFlow(sw, filteringMatch, msg.getBufferId(), bestDeviceInfo.getPort(), actionList);
    }

    private Match filteringMatch(IOFSwitch sw, OFPacketIn msg, Ethernet ethDataIn, IPv4 iPv4Packet) {
        return sw.getOFFactory().buildMatch()
                .setExact(MatchField.ETH_TYPE, EthType.IPv4)
                .setExact(MatchField.IN_PORT, OFMessageUtils.getInPort(msg))
                .setExact(MatchField.IPV4_SRC, iPv4Packet.getSourceAddress())
                .setExact(MatchField.ETH_SRC, ethDataIn.getSourceMACAddress())
                .build();
    }

    private void injectSourceChangingFlow(IOFSwitch sw, OFPacketIn msg, Ethernet ethDataIn) {
        final IPv4 iPv4Packet = (IPv4) ethDataIn.getPayload();
        final DeviceInfo destDeviceInfo = hostNodesToDeviceInfoMap.get(iPv4Packet.getDestinationAddress().toString());

        final Match filteringMatch = filteringMatch(sw, msg, ethDataIn, iPv4Packet);
        final List<OFAction> actionList = ActionsProvider.getSourceChangingActions(EXTERNAL_ADDRESS,
                cdNodesToDeviceInfoMap.get(EXTERNAL_ADDRESS).getMac(), destDeviceInfo.getPort(), sw.getOFFactory());

        flowInjectorService.injectFlow(sw, filteringMatch, msg.getBufferId(), destDeviceInfo.getPort(), actionList);
    }

    private boolean fromInternalAddress(Ethernet ethData) {
        if(ethData.getEtherType() == EthType.IPv4) {
            final IPv4 iPv4 = (IPv4) ethData.getPayload();
            return isWithinInternal(iPv4.getSourceAddress().toString());
        }
        return false;
    }

    @Override
    public Collection<Class<? extends IFloodlightService>> getModuleServices() {
        return null;
    }

    @Override
    public Map<Class<? extends IFloodlightService>, IFloodlightService> getServiceImpls() {
        return null;
    }

    @Override
    public Collection<Class<? extends IFloodlightService>> getModuleDependencies() {
        return Collections.singletonList(IFloodlightProviderService.class);
    }

    @Override
    public void init(FloodlightModuleContext context) throws FloodlightModuleException {
        floodlightProviderService = context.getServiceImpl(IFloodlightProviderService.class);
    }

    @Override
    public void startUp(FloodlightModuleContext context) throws FloodlightModuleException {
        floodlightProviderService.addOFMessageListener(OFType.PACKET_IN, this);
        logger.info("Starting LoadBalancerModule");
    }
}