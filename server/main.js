var express = require('express')
var app = express()

app.get('/', function (req, res) {
    res.json({"value":  Math.ceil(Math.random() * 100) });
});

app.listen(3000, function () {
    console.log('Random number generator listening on port 3000!')
});