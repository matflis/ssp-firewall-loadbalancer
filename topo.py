from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import RemoteController
from mininet.cli import CLI

hosts_num = 2
cds_num = 3
services = {
    'WWW' : 80,
    'PORN' : 11069,
    'NETFLIX' : 11070,
    'GOOGLE' : 11071
}


class SdnTopo(Topo):
    def __init__(self):
        Topo.__init__(self)
	
        print "Adding hosts"
        user_hosts = [self.addHost('h%s' % i, ip='10.0.0.%s/24'% i, mac='00:00:00:00:00:0%s' % i) for i in range(1, hosts_num + 1)]

        print "Hosts added. Adding CD hosts"
        cd_hosts = [self.addHost('cd%s' % i, ip='10.0.0.1%s' % i, mac='00:00:00:00:00:1%s' % i) for i in range(1, cds_num + 1)]

        print "CD hosts added. Adding switch"
        switch = self.addSwitch('s1')
        print "Switch added. Creating connections"

        for i in range(hosts_num):
            self.addLink(user_hosts[i], switch)

        for i in range(cds_num):
            self.addLink(cd_hosts[i], switch)
        print "Connections established"


def run():
    print "Creating topology"
    topo = SdnTopo()
    print "Topology created"

    print "Starting network"
    net = Mininet(topo=topo, build = False)
    net.addController(controller = RemoteController, ip = "127.0.0.1", port = 6653)
    net.build()
    net.start()
    print "Network started"

    for i in range(cds_num):
	print "Running services for node %s" % (i+ 1)
        cd_host = net.get("cd%s" % (i + 1))
        for service in services:
	    service_port = services[service]
            print "Running service '%s' on port '%s'" % (service, service_port) 
            cd_host.cmd("python -m SimpleHTTPServer %s &" % service_port)

    print "Services started. All up & ready"
    CLI(net)
    net.stop()

if __name__ == '__main__':
    run()

